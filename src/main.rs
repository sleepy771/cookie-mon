extern crate actix;
extern crate actix_web;
extern crate actix_redis;
extern crate cookie;
extern crate http;
extern crate kafka;

extern crate serde;
extern crate serde_json;

#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate log;

extern crate log4rs;
extern crate argparse;
extern crate config;

mod conf;
mod logging;

use actix_redis::RedisSessionBackend;
use actix_web::{server, App, HttpRequest, HttpResponse};
use actix_web::middleware::session::RequestSession;
use http::header::LOCATION;
use http::uri::Uri;
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use kafka::producer::Record;
use std::sync::Mutex;
use conf::{load_settings, CMConfig};
use std::str::FromStr;
use std::fmt;

pub const COOKIE_CHALLENGE: &str = "Cookie-Challenge";
const ADDRESS: &str = "Address";
const ORIGINAL_URI: &str = "OriginalUri";
const VERIFY_ADDRESS: &str = "/verify";


type IPAddress = String;
type LogMessage = String;
type Remote = String;


lazy_static! {
    static ref CONFIG: CMConfig = load_settings();
}

lazy_static! {
    static ref PRODUCER: Mutex<ChallengeLogger> = Mutex::new(ChallengeLogger::init());
}


fn verify_challenge(request: HttpRequest) -> HttpResponse {
    let connection_info = request.connection_info();
    let uri = request.uri();

    match connection_info.remote() {
        Some(remote_str) => {
            let remote: String = remote_str.to_owned();

            match request.session().get::<String>(ADDRESS) {
                Ok(address_option) => {
                    match address_option {
                        Some(address) => {
                            if address == remote {
                                let debug_message = format!("Remote `{}` successfully authenticated, to uri: `{}`", &remote, uri);
                                log_event(LogResult::Successful(
                                    uri.clone(),
                                    remote.clone(),
                                    debug_message
                                ));
                                match request.session().get::<String>(ORIGINAL_URI) {
                                    Ok(uri_option) => {
                                        match uri_option {
                                            Some(uri) => {
                                                return HttpResponse::MovedPermanently().header(LOCATION, uri).finish();
                                            },
                                            None => {
                                                error!("Address for remote: `{}` not found!", remote);
                                                return HttpResponse::BadRequest().finish();
                                            }
                                        }
                                    },
                                    Err(err) => {
                                        error!("An error occured during obtaining original url:\n{}", err);
                                        return HttpResponse::MovedPermanently().header(LOCATION, CONFIG.get_default_redirect()).finish()
                                    }
                                };
                            } else {
                                let debug_message = format!("Remote `{}` connecting to uri: `{}` failed challenge. Previous remote: `{}`", &remote, uri, &address);
                                log_event(
                                    LogResult::Failed(
                                        uri.clone(),
                                        remote.clone(),
                                        address,
                                        debug_message
                                    )
                                );
                                return HttpResponse::TemporaryRedirect().header(LOCATION, "/").finish();
                            }
                        },
                        None => {
                            let debug_message = format!("Cookie-mon can not verify user: `{}`. REDIRECTING...", &remote);
                            log_event(
                                LogResult::RedirectingBack(
                                    remote.clone(),
                                    debug_message
                                )
                            );
                            return HttpResponse::TemporaryRedirect().header(LOCATION, "/").finish();
                        }
                    }
                },
                Err(err) => {
                    error!("Redis connection failed, due to:\n{}", err);
                    return HttpResponse::InternalServerError().finish();
                }
            }
        },
        None => {
            let debug_message = "Cookie-mon encountered request with unspecified remote, heading to: /verify".to_owned();
            log_event(LogResult::Blind(
                uri.clone(),
                debug_message
            ));
            return HttpResponse::BadRequest().finish();
        }
    }
}

fn challenge(request: HttpRequest) -> HttpResponse {
    let connection_info = request.connection_info();
    let uri = request.uri();

    match connection_info.remote() {
        Some(remote_str) => {
            let remote: String = remote_str.to_owned();

            match request.session().get::<String>(ADDRESS) {
                Ok(address_option) => {
                    match address_option {
                        Some(_address) => {
                            warn!("Client: `{}` accessed challenge page multiple times!", &remote);
                        },
                        None => {
                            let debug_message = format!("Cookie-mon challenged remote: `{}`", &remote);
                            log_event(
                                LogResult::Challenged(
                                    uri.clone(),
                                    remote.clone(),
                                    debug_message
                                )
                            );
                            match request.session().set(ADDRESS, &remote) {
                                Ok(_) => {},
                                Err(err) => {
                                    error!("Can not create session variable address for remote: {}, due to:\n{}", &remote, err);
                                    return HttpResponse::InternalServerError().finish();
                                }
                            };
                            match request.session().set(ORIGINAL_URI, format!("{}", &uri)) {
                                Ok(_) => {},
                                Err(err) => warn!("Can not create session variable ORIGINAL_URI: `{}` for remote {}, due to:\n{}", &uri, &remote, err)
                            }
                        }
                    }
                    return HttpResponse::TemporaryRedirect().header(LOCATION, VERIFY_ADDRESS).finish();
                },
                Err(err) => {
                    error!("Redis connection failed, due to:\n{}", err);
                    return HttpResponse::InternalServerError().finish();
                }
            }
        },
        None => {
            let debug_message = format!("Cookie-mon encountered request with unspecified remote, heading to: `{:?}`", uri);
            log_event(LogResult::Blind(
                uri.clone(),
                debug_message
            ));
            return HttpResponse::Forbidden().finish();
        }
    }

}

#[derive(Debug,Eq,PartialEq)]
enum LogResult {
    Blind(Uri, LogMessage),
    Challenged(Uri, Remote, LogMessage),
    Successful(Uri, Remote, LogMessage),
    Failed(Uri, Remote, IPAddress, LogMessage),
    RedirectingBack(Remote, LogMessage)
}

fn log_event(log: LogResult) -> () {
    debug!("Sending log event {:?} to kafka ...", log);
    match PRODUCER.lock() {
        Ok(ref mut guard) => {
            guard.log_event(log);
        },
        Err(err) => error!("Can not obtain mutex guard, due to:\n{}", err)
    };
    debug!("Event sent successfully!")
}

struct ChallengeLogger {
    kafka_producer: kafka::producer::Producer,
}

#[derive(Serialize,Deserialize,Debug)]
struct RemoteUri {
    scheme: Option<String>,
    authority: Option<String>,
    path: String,
    query: Option<String>,
}


impl RemoteUri {
    fn from(url: &Uri) -> RemoteUri {
        RemoteUri {
            scheme: match url.scheme_part() {
                Some(scheme) => Some(scheme.as_str().to_owned()),
                None => None
            },
            authority: match url.authority_part() {
                Some(auth) => Some(auth.as_str().to_owned()),
                None => None
            },
            path: url.path().to_owned(),
            query: match url.query() {
                Some(query) => Some(query.to_owned()),
                None => None
            },
        }
    }
}


impl fmt::Display for RemoteUri {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut parts: Vec<String> = Vec::with_capacity(5);
        if let Some(scheme) = &self.scheme {
            parts.push(format!("{}://", scheme));
        }
        if let Some(auth) = &self.authority {
            parts.push(auth.clone());
        }
        parts.push(self.path.clone());
        if let Some(query) = &self.query {
            parts.push(format!("?{}", query));
        }

        write!(f, "{}", parts.join(""))
    }
}


#[derive(Serialize,Deserialize,Debug)]
struct RemoteLogMessage {
    remote: Option<String>,
    previous: Option<String>,
    uri: RemoteUri,
    time_ms: u64,
    message: Option<String>,
    kind: String,
}

impl RemoteLogMessage {

    fn new_now(kind: &str, uri: Uri) -> RemoteLogMessage {
        RemoteLogMessage::new(kind, uri, None)
    }

    fn new(kind: &str, uri: Uri, time: Option<u64>) -> RemoteLogMessage {
        let unix_time = match time {
            Some(t) => t,
            None => {
                let dur = SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards!");
                dur.as_secs() * 1000_000_000 + (dur.subsec_nanos() as u64)
            }
        };
        RemoteLogMessage {
            remote: None,
            previous: None,
            uri: RemoteUri::from(&uri),
            time_ms: unix_time,
            message: None,
            kind: kind.to_owned(),
        }
    }

    fn set_remote(mut self, remote: String) -> RemoteLogMessage {
        self.remote = Some(remote);
        self
    }

    fn set_previous(mut self, previous: String) -> RemoteLogMessage {
        self.previous = Some(previous);
        self
    }

    fn set_message(mut self, message: String) -> RemoteLogMessage {
        self.message = Some(message);
        self
    }

    fn as_json(&self) -> Result<String, String> {
        match serde_json::to_string(self) {
            Ok(serialized) => Ok(serialized),
            Err(e) => Err(format!("RemoteLogMessage object: `{:?}` can not be converted to json. Reason:\n{}", self, e))
        }
    }
}

impl ChallengeLogger {
    fn init() -> ChallengeLogger {
        ChallengeLogger {
            kafka_producer: {
                let mut prod_builder = kafka::producer::Producer::from_hosts(CONFIG.get_kafka().bootstrap_servers())
                    .with_ack_timeout(Duration::from_secs(CONFIG.get_kafka().ack_timeout()))
                    .with_required_acks(CONFIG.get_kafka().required_acks().unwrap());

                prod_builder = match CONFIG.get_kafka().client_id() {
                    Some(client_id) => prod_builder.with_client_id(client_id),
                    None => prod_builder
                };

                prod_builder = match CONFIG.get_kafka().compression() {
                    Ok(compression) => prod_builder.with_compression(compression),
                    Err(e) => {
                        error!("Failed to load compression setting, due to:\n{}.\nUsing default `none`.", e);
                        prod_builder
                    }
                };

                prod_builder.create().unwrap()
            }
        }
    }

    pub fn log_event(&mut self, result: LogResult) {
        match result {
            LogResult::Blind(uri, message) => {
                let log_message = RemoteLogMessage::new_now("BLIND", uri)
                    .set_message(message);
                self.send_log_message("BLIND", log_message);
            },
            LogResult::Challenged(uri, address, message) => {
                let log_message = RemoteLogMessage::new_now(
                    "CHALLENGED",
                    uri)
                    .set_remote(address)
                    .set_message(message);
                self.send_log_message("CHALLENGED", log_message);
            },
            LogResult::Successful(uri, address, message) => {
                let log_message = RemoteLogMessage::new_now("SUCCESSFUL", uri)
                    .set_remote(address.clone())
                    .set_message(message);
                self.send_log_message("SUCCESSFUL", log_message);
                self.send_challenged(address);
            },
            LogResult::Failed(uri, address, previous, message) => {
                let log_message = RemoteLogMessage::new_now("FAILED", uri)
                    .set_remote(address)
                    .set_previous(previous)
                    .set_message(message);
                self.send_log_message("FAILED", log_message);
            },
            LogResult::RedirectingBack(remote, message) => {
                let log_message = RemoteLogMessage::new_now("REDIRECT_BACK", Uri::from_str("/verify").unwrap())
                    .set_message(message)
                    .set_remote(remote);
                self.send_log_message("REDIRECT_BACK", log_message);
            }
        };
    }

    fn send_log_message(&mut self, key: &str, message: RemoteLogMessage) {
        match message.as_json() {
            Ok(json_str) => {
                match self.kafka_producer.send(&Record::from_key_value("cm.log", key, json_str)) {
                    Ok(()) => {},
                    Err(e) => error!("Following error occurred during send:\n{}", e)
                }
            },
            Err(e) => {
                error!("Following error occurred during send:\n{}", e)
            }
        };
    }

    fn send_challenged(&mut self, value: String) {
        match self.kafka_producer.send(&Record::from_value("cm.challenged", value.clone())) {
            Ok(()) => {},
            Err(err) => error!("Can not send challenged address via kafka: `{}`, due to:\n{}", value, err)
        }
    }
}

fn main() {
    logging::init_logger();
    let sys = actix::System::new("cm-challenge");

    server::new(|| {
        App::new()
            .middleware(actix_web::middleware::Logger::default())
            .middleware(actix_web::middleware::session::SessionStorage::new(
            RedisSessionBackend::new(CONFIG.redis().host(), &[0; 32])
                .ttl(CONFIG.get_cookie_ttl())
                .cookie_name(CONFIG.get_cookie_name())))
            .resource("/", |r| r.f(challenge))
            .resource(VERIFY_ADDRESS, |r| r.f(verify_challenge))
    }).bind(CONFIG.get_host()).unwrap().start();

    let _ = sys.run();
}
