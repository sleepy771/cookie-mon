use std::default::Default;
use kafka::producer::RequiredAcks;
use kafka::producer::Compression;
use argparse;
use config;
use std::path;
use std::env;


const GLOBAL_CONFIG_PATH: &str = "/etc/CMChallenger/settings.toml";
const ENVIRONMENT_PREFIX: &str = "CMCH";
const LOCAL_CONFIG_PATH_SUFFIX: &str = ".CMChallenger/settings.toml";


#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct CMConfig {
    cookie_ttl: u16,
    cookie_name: String,
    default_redirect: String,
    host: String,
    redis: RedisConf,
    kafka: KafkaConf,
}

impl CMConfig {
    pub fn get_cookie_ttl(&self) -> u16 {
        self.cookie_ttl
    }

    pub fn get_cookie_name(&self) -> &str {
        self.cookie_name.as_ref()
    }

    pub fn get_host(&self) -> &str {
        self.host.as_ref()
    }

    pub fn get_default_redirect(&self) -> String {
        self.default_redirect.clone()
    }

    pub fn get_kafka(&self) -> &KafkaConf {
        &self.kafka
    }

    pub fn redis(&self) -> &RedisConf {
        &self.redis
    }
}


impl Default for CMConfig {
    fn default() -> Self {
        CMConfig {
            cookie_ttl: 20,
            cookie_name: ::COOKIE_CHALLENGE.to_owned(),
            default_redirect: "http://localhost:80/".to_owned(),
            host: "0.0.0.0:8888".to_owned(),
            redis: RedisConf::default(),
            kafka: KafkaConf::default()
        }
    }
}


#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct RedisConf {
    host: String,
}

impl RedisConf {
    pub fn host(&self) -> String {
        self.host.clone()
    }
}


impl Default for RedisConf {
    fn default() -> Self {
        RedisConf {
            host: "redis://127.0.0.1:6379".to_owned()
        }
    }
}


#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct KafkaConf {
    bootstrap_servers: Vec<String>,
    ack_timeout: u64,
    required_acks: String,
    compression: Option<String>,
    client_id: Option<String>,
}

impl KafkaConf {
    pub fn bootstrap_servers(&self) -> Vec<String> {
        self.bootstrap_servers.clone()
    }

    pub fn ack_timeout(&self) -> u64 {
        self.ack_timeout
    }

    pub fn required_acks(&self) -> Result<RequiredAcks, String> {
        match self.required_acks.to_ascii_lowercase().as_ref() {
            "none" => Ok(RequiredAcks::None),
            "one" => Ok(RequiredAcks::One),
            "all" => Ok(RequiredAcks::All),
            other => Err(format!("Unknown `required_acks` type: `{}`. Available options are: [`none`, `one`, `all`].", other))
        }
    }

    pub fn compression(&self) -> Result<Compression, String> {
        match &self.compression {
            Some(compression) => {
                match compression.to_ascii_lowercase().as_ref() {
                    "none" => Ok(Compression::NONE),
                    "gzip" => Ok(Compression::GZIP),
                    "snappy" => Ok(Compression::SNAPPY),
                    other => Err(format!("Unknown `kafka.compression` type: `{}`. Allowed types are: [`none`, `gzip`, `snappy`]", other))
                }
            },
            None => Ok(Compression::NONE)
        }
    }

    pub fn client_id(&self) -> Option<String> {
        self.client_id.clone()
    }
}

impl Default for KafkaConf {
    fn default() -> Self {
        KafkaConf {
            bootstrap_servers: vec!["localhost:9092".to_owned()],
            ack_timeout: 1,
            required_acks: "one".to_owned(),
            compression: None,
            client_id: None
        }
    }
}

pub struct CmdArgs {
    cookie_ttl: Option<u16>,
    cookie_name: Option<String>,
    host: Option<String>,
    default_redirect: Option<String>,
    redis_host: Option<String>,
    kafka_bootstrap_servers: Option<String>,
    kafka_ack_timeout: Option<u64>,
    kafka_required_acks: Option<String>,
    kafka_compression: Option<String>,
    kafka_client_id: Option<String>,
    config_file: Option<String>,
    logger_config: Option<String>
}

impl CmdArgs {
    pub fn get_cookie_ttl(&self) -> Option<u16> {
        self.cookie_ttl
    }

    pub fn get_cookie_name(&self) -> Option<String> {
        self.cookie_name.clone()
    }

    pub fn get_host(&self) -> Option<String> {
        self.host.clone()
    }

    pub fn get_redis_host(&self) -> Option<String> {
        self.redis_host.clone()
    }

    pub fn get_kafka_bootstrap_servers(&self) -> Option<Vec<String>> {
        match &self.kafka_bootstrap_servers {
            Some(bootstrap_srvs) => {
                Some(bootstrap_srvs.split(",").map(|e| e.to_owned() ).collect::<Vec<String>>())
            },
            None => None
        }
    }

    pub fn get_ack_timeout(&self) -> Option<u64> {
        self.kafka_ack_timeout
    }

    pub fn get_required_acks(&self) -> Option<String> {
        self.kafka_required_acks.clone()
    }

    pub fn get_kafka_compression(&self) -> Option<String> {
        self.kafka_compression.clone()
    }

    pub fn get_kafka_client_id(&self) -> Option<String> {
        self.kafka_client_id.clone()
    }

    pub fn get_config_file(&self) -> Option<String> {
        self.config_file.clone()
    }

    pub fn get_logger_config(&self) -> Option<String> {
        self.logger_config.clone()
    }

    pub fn get_default_redirect(&self) -> Option<String> {
        self.default_redirect.clone()
    }

    pub fn overwrite(&self, mut config: CMConfig) -> CMConfig {
        if let Some(ttl) = self.get_cookie_ttl() {
            config.cookie_ttl = ttl;
        }
        if let Some(name) = self.get_cookie_name() {
            config.cookie_name = name;
        }
        if let Some(host) = self.get_host() {
            config.host = host;
        }
        if let Some(host) = self.get_redis_host() {
            config.redis.host = host;
        }
        if let Some(srvs) = self.get_kafka_bootstrap_servers() {
            config.kafka.bootstrap_servers = srvs;
        }
        if let Some(timeout) = self.get_ack_timeout() {
            config.kafka.ack_timeout = timeout;
        }
        if let Some(req_acks) = self.get_required_acks() {
            config.kafka.required_acks = req_acks;
        }
        if let Some(compression) = self.get_kafka_compression() {
            config.kafka.compression = Some(compression);
        }
        if let Some(cli_id) = self.get_kafka_client_id() {
            config.kafka.client_id = Some(cli_id);
        }
        if let Some(default_red) = self.get_default_redirect() {
            config.default_redirect = default_red;
        }

        config
    }
}

pub fn parse_console_args() -> CmdArgs {
    let mut args = CmdArgs {
        cookie_ttl: None,
        cookie_name: None,
        default_redirect: None,
        host: None,
        redis_host: None,
        kafka_bootstrap_servers: None,
        kafka_ack_timeout: None,
        kafka_required_acks: None,
        kafka_compression: None,
        kafka_client_id: None,
        config_file: None,
        logger_config: None,
    };
    {
        let mut ap = argparse::ArgumentParser::new();
        ap.set_description("Cookie-mon is WebApplication challenging incomming http/s connections.");
        ap.refer(&mut args.config_file)
            .add_option(&["-c", "--config"], argparse::StoreOption, "Alternative path to cookie-mon config file.");
        ap.refer(&mut args.logger_config)
            .add_option(&["-l", "--logger-config"], argparse::StoreOption, "Alternative path to log4rs config file.");
        ap.refer(&mut args.host)
            .add_option(&["-H", "--host"], argparse::StoreOption, "Host where should cookie-mon listen (i.e. `10.20.30.101:8000`). Defaults to `0.0.0.0:8888`.");
        ap.refer(&mut args.redis_host)
            .add_option(&["-r", "--redis-host"], argparse::StoreOption, "Redis service host (i.e. `unix:///tmp/redis.sock`).");
        ap.refer(&mut args.kafka_bootstrap_servers)
            .add_option(&["-k", "--kafka-bootstrap-servers"], argparse::StoreOption, "Kafka bootstrap servers. Format: localhost:9092,10.20.30.40:9092,...");
        ap.refer(&mut args.kafka_ack_timeout)
            .add_option(&["-a", "--kafka-ack-timeout"], argparse::StoreOption, "Kafka producer ack timeout.");
        ap.refer(&mut args.kafka_compression)
            .add_option(&["-m", "--kafka-compression"], argparse::StoreOption, "Kafka compression. Available formats are: [`none`,`gzip`,`snappy`].");
        ap.refer(&mut args.kafka_client_id)
            .add_option(&["-i", "--kafka-client-id"], argparse::StoreOption, "Kafka producer client id.");
        ap.refer(&mut args.default_redirect)
            .add_option(&["-d", "--default-redirect"], argparse::StoreOption, "Default host where should be redirected users that overcome challenge.");
        ap.parse_args_or_exit();
    };
    args
}


pub fn load_settings() -> CMConfig {
    let cmd_args = parse_console_args();

    let settings = match cmd_args.get_config_file() {
        Some(cfg_file) => {
            let mut settings = config::Config::default();
            settings.merge(config::File::from(path::PathBuf::from(cfg_file))).unwrap();
            settings
        },
        None => {
            let mut settings = config::Config::default();
            match env::home_dir() {
                Some(home_dir) => {
                    match settings.merge(config::File::from(path::PathBuf::from(home_dir).join(path::Path::new(LOCAL_CONFIG_PATH_SUFFIX)))) {
                        Ok(_) => {},
                        Err(err) => warn!("Can not find local config ... attempt to use global config.\nErr: {}", err)
                    };
                }
                None => {
                    warn!("$HOME dir doesn't seem to be supported by host operating system.");
                }
            }
            let global_config = path::PathBuf::from(GLOBAL_CONFIG_PATH.to_owned());
            match settings.merge(config::File::from(global_config)) {
                Ok(_) => {},
                Err(err) => warn!("Can not find global config ... attempt to use environment variables.\nErr: {}", err) 
            };
            match settings.merge(config::Environment::with_prefix(ENVIRONMENT_PREFIX)) {
                Ok(_) => {},
                Err(err) => error!("Can not use environement variables due to: {}. Falling back to defaults.", err)
            }
            settings
        }
    };
    let mut cfg = CMConfig::default();

    if let Ok(host) = settings.get::<String>("host") {
       cfg.host = host;
    }

    if let Ok(redis_host) = settings.get::<String>("redis_host") {
        cfg.redis.host = redis_host;
    }

    if let Ok(bootstrap) = settings.get::<Vec<String>>("kafka_bootstrap_servers") {
        cfg.kafka.bootstrap_servers = bootstrap;
    }

    if let Ok(ack_timeout) = settings.get::<u64>("kafka_ack_timeout") {
        cfg.kafka.ack_timeout = ack_timeout;
    }

    if let Ok(compression) = settings.get::<String>("kafka_compression") {
        cfg.kafka.compression = Some(compression);
    }

    if let Ok(client_id) = settings.get::<String>("kafka_client_id") {
        cfg.kafka.client_id = Some(client_id);
    }

    if let Ok(default_red) = settings.get::<String>("default_redirect") {
        cfg.default_redirect = default_red;
    }

    cmd_args.overwrite(cfg)
}

